<<<<<<< HEAD
/*jslint
    browser: true,
    node: true,
 */
/*globals
    MutationObserver,
    document
 */
"use strict";

function $(id) {
    return document.getElementById(id);
}

function Element(valor, clase) {
    this.valor = valor;
    this.clase = clase;
}
var ns = (function (ns) {
    ns.INPUTS = ["valor", "clase"];
    ns.PLACEHOLDER = "Introduzca ";
    ns.TITLE = "Manipulación de documentos a través del DOM";
    ns.SELECT_TITLE = "Lista de elementos";
    ns.ADD_BUTTON_VALUE = "Añadir";
    ns.DELETE_BUTTON_VALUE = "Eliminar";
    ns.TABLE_BUTTON_VALUE = "Tabla";
    ns.INPUTS_WITHOUT_VALUE = "Tiene que rellenar ambos campos";
    ns.NO_INDEX_SELECTED = "No hay ninguna clase para borrar";
    ns.OBJECTS = [];

    ns.objectCount = function (clase, valor) {
        return ns.OBJECTS.reduce(function (x, y) {
            return (y.clase === clase && y.valor === valor) ? x + 1 : x;
        }, 0);
    };
    ns.changeColors = function (td, numOfObjects) {
        var color;
        if (numOfObjects < 2) {
            color = "red";
        } else {
            if (numOfObjects > 2) {
                color = "green";
            } else {
                color = "inherit";
            }
        }
        td.style.color = color;
    };
    ns.reloadTable = function () {
        var divTable = $("listaElementos").cloneNode(false),
            newTable = ns.create("table", "lista", "", "", divTable),
            clases = ns.unique("clase"),
            values = ns.unique("valor"),
            headers = ns.create("tr", "headers", "", "", newTable),
            tr,
            numOfObjects,
            td;
        ns.create("td", "", "", "", headers);
        clases.forEach(function (x) {
            ns.create("td", "", "", x, headers);
        });

        values.forEach(function (value) {
            tr = ns.create("tr", "", "", value, newTable);
            clases.forEach(function (clase) {
                numOfObjects = ns.objectCount(clase, value);
                td = ns.create("td", "", "", numOfObjects, tr);
                ns.changeColors(td, numOfObjects);
            });
        });
        $("divLista").replaceChild(divTable, $("listaElementos"));
    };
    ns.reloadList = function () {
        var divUl = $("listaElementos").cloneNode(false),
            newUl = ns.create("ul", "lista", "", "", divUl);
        ns.OBJECTS.forEach(function (x) {
            ns.create("li", "", x.clase, x.valor, newUl);
        });
        $("divLista").replaceChild(divUl, $("listaElementos"));
    };
    ns.unique = function (type) {
        return ns.OBJECTS.map(function (x) {
            return x[type];
        }).filter(function (element, index, array) {
            return index === array.indexOf(element);
        });
    };
    ns.reloadData = function () {
        return $("tableList").value === "Tabla" ? ns.reloadList() : ns.reloadTable();
    };
    ns.create = function (tag, id, clase, text, parent) {
        var element = document.createElement(tag);
        element.id = id;
        element.className = clase;
        element.appendChild(document.createTextNode(text));
        parent.appendChild(element);
        return element;
    };
    ns.save = function (valor, clase) {
        return ns.OBJECTS.push(new Element(valor, clase));
    };
    ns.delete = function (clase) {
        ns.OBJECTS = ns.OBJECTS.filter(function (x) {
            return x.clase !== clase;
        });
    };
    ns.selectAll = function (tag) {
        return Array.from(document.querySelectorAll(tag));
    };
    ns.selectFirst = function (tag) {
        return Array.from(document.querySelector(tag));
    };
    ns.changeDataView = function () {
        if ($("tableList").value === "Tabla") {
            ns.reloadTable();
            $("tableList").value = "Lista";
        } else {
            ns.reloadList();
            $("tableList").value = "Tabla";
        }
    };
    return ns;
}({}));

function setTitle() {
    ns.create("h1", "title", "", ns.TITLE, document.body);
}
=======
window.onload = function () {
    "use strict";
    //document.writeln("");
    var doctype = document.implementation.createDocumentType('html', '', '');
    document.insertBefore(doctype, document.firstChild);

    function addCharset() {
        var meta = document.createElement("meta");
        meta.setAttribute("charset", "utf-8");
        document.head.insertBefore(meta, document.head.firstChild);
    }

    var ns = (function (ns) {
        ns.BODY = document.body;
        ns.INPUTS = ["valor", "clase"];
        ns.ADD_BUTTON = document.createElement("input");
        ns.SELECT = document.getElementsByTagName("select")[0];
        //ns.UL = document.querySelector("ul");
        ns.PLACEHOLDER = "Introduzca ";
        ns.TITLE = "Manipulación de documentos a través del DOM";
        ns.NO_INDEX_SELECTED = "No hay ninguna clase seleccionada";
        ns.INPUT_WITHOUT_VALUE = "Tiene que rellenar ambos campos";
        ns.Objects = [];

        ns.create = function (tag, text, clase, parent) {
            var element = document.createElement(tag);
            element.appendChild(document.createTextNode(text));
            element.className = clase;
            parent.appendChild(element);
        }
        ns.save = function (element) {
            ns.Objects.push(element);
        };

        ns.selectFirst = function (tag) {
            return document.querySelector(tag);
        };

        ns.selectAll = function (tag) {
            //Array.from();
            //$$() DEVUELVE ARRAY
            return Array.prototype.slice.call(document.querySelectorAll(tag));
        };
        ns.createError = function (msg) {
            var h3 = document.createElement("h3");
            h3.innerHTML = msg;
            ns.selectFirst("form").appendChild(h3);
        };

        ns.reloadList = function () {
            array.from(ns.LIST.children).forEach(function (x) {
                x.remove();
            })
            ns.Objects.forEach(function (x) {
                ns.create("li", x.valor, x.clase, document.getElementsByTagName("ul")[0]);
            });
        };
>>>>>>> b02033ea6498be1b84c08751c38365aa5afb6d78

function appendOption(objects) {
    //Con true coge todos sus children
    var newSelect = document.getElementsByTagName("select")[0].cloneNode(false);
    objects.forEach(function (x) {
        ns.create("option", "", x, x, newSelect);
    });
    $("divLista2").replaceChild(newSelect, $("classSelect"));
}

<<<<<<< HEAD
function deleteSelectedOption() {
    var select = document.getElementsByTagName("select")[0],
        selected = select.selectedIndex;
    if (selected !== -1) {
        ns.delete(select[selected].className);
        appendOption(ns.unique("clase"));
    } else {
        ns.create("p", "", "", ns.NO_INDEX_SELECTED, $("form"));
=======
    function Element(valor, clase) {
        this.valor = valor;
        this.clase = clase;
    }

    function setTitle() {
        ns.create("h1", ns.TITLE, ns.BODY);
    }

    function removeDeleteClassError() {
        ns.selectFirst("h3") ? ns.selectFirst("h3").remove() : 0;
>>>>>>> b02033ea6498be1b84c08751c38365aa5afb6d78
    }
}

<<<<<<< HEAD
function createDeleteAndTableButtons() {
    var deleteBtn = ns.create("input", "delete", "", "Eliminar", $("divLista")),
        tableBtn;
    deleteBtn.setAttribute("type", "button");
    tableBtn = ns.create("input", "tableList", "", "Tabla", $("divLista"));
    deleteBtn.setAttribute("type", "button");
    tableBtn.setAttribute("type", "button");
    deleteBtn.value = ns.DELETE_BUTTON_VALUE;
    tableBtn.value = ns.TABLE_BUTTON_VALUE;
    deleteBtn.addEventListener("click", deleteSelectedOption, false);
    tableBtn.addEventListener("click", ns.changeDataView, false);
}

function insertIntoList() {
    var valueInput = document.forms[0].children[0].value,
        classInput = document.forms[0].children[1].value;
    if (valueInput !== "" && classInput !== "") {
        ns.save(valueInput, classInput);
        appendOption(ns.unique("clase"));
=======
    function insertIntoList() {
        var valueInput = document.forms[0].children[0].value,
            classInput = document.forms[0].children[1].value;

        if (valueInput !== "" && classInput !== "") {
            ns.save(new Element(valueInput, classInput));

            /*ul = document.getElementsByTagName("ul")[0],
        li = document.createElement("li");*/
            ns.reloadList();
        } else {
            ns.createError(ns.INPUT_WITHOUT_VALUE);
        }
>>>>>>> b02033ea6498be1b84c08751c38365aa5afb6d78
        document.forms[0].reset();
    } else {
        ns.create("p", "", "", ns.INPUTS_WITHOUT_VALUE, $("form"));
    }
}

<<<<<<< HEAD
function createSelectAndList() {
    var div = ns.create("div", "divLista", "", "", document.body),
        div2 = ns.create("div", "divLista2", "", "", div);
    ns.create("h2", "listaTitle", "", ns.SELECT_TITLE, $("divLista"));
    ns.create("select", "classSelect", "", "", div2);

    ns.create("div", "listaElementos", "", "", $("divLista"));
    ns.create("ul", "lista", "", "", $("listaElementos"));
    createDeleteAndTableButtons();
}
=======
    function createClassForm() {
        var form = document.createElement("form");

        ns.INPUTS.forEach(function (x) {
            var element = document.createElement("input");
            element.placeholder = ns.PLACEHOLDER + x;
            form.appendChild(element);
        });
        ns.ADD_BUTTON.setAttribute("type", "button");
        ns.ADD_BUTTON.value = "Añadir";
        ns.ADD_BUTTON.addEventListener("click", insertIntoList, false);

        form.appendChild(ns.ADD_BUTTON);
        ns.BODY.appendChild(form);
        createSelectAndList();
    }

    function deleteOption() {
        ns.selectFirst("." + ns.SELECT.children[selected].value).remove();
        select.removeChild(select.children[selected]);
        changeBackgroundOfListItem();
    }

    function deleteSelectedOption() {
        var selected = ns.selectFirst("select").selectedIndex;
        selected !== -1 ? deleteOption(selected) : ns.createError(ns.NO_INDEX_SELECTED);
    }

    function createDeleteButton() {
        var deleteBtn = document.createElement("input");
        deleteBtn.setAttribute("type", "button");
        deleteBtn.value = "Eliminar";
        ns.selectFirst("div").appendChild(deleteBtn);
        ns.selectFirst("div").lastChild.addEventListener("click", deleteSelectedOption, false);
    }

    function createSelectAndList() {
        var select = document.createElement("select"),
            h2 = document.createElement("h2"),
            div = document.createElement("div");
        h2.appendChild(document.createTextNode("Lista de elementos"));
        div.appendChild(h2);
        div.appendChild(document.createElement("ul"));
        div.appendChild(select);
        ns.BODY.appendChild(div)
        createDeleteButton();
    }
>>>>>>> b02033ea6498be1b84c08751c38365aa5afb6d78

function createClassForm() {
    var form = ns.create("form", "form", "", "", document.body),
        button,
        input;
    ns.INPUTS.forEach(function (x) {
        input = ns.create("input", x, "", "", $("form"));
        input.placeholder = ns.PLACEHOLDER + x;
    });
    button = ns.create("input", "add", "", "Añadir", form);
    button.setAttribute("type", "button");
    button.value = ns.ADD_BUTTON_VALUE;
    button.addEventListener("click", insertIntoList, false);
    createSelectAndList();
}

<<<<<<< HEAD
function removeDeleteClassError() {
    if (ns.selectAll("p")) {
        Array.from(ns.selectAll("p")).forEach(function (x) {
            x.remove();
        });
    }
}

function createPage() {
    setTitle();
    createClassForm();
    var config = {
            attributes: true,
            childList: true,
            characterData: true
        },
        observer;
    observer = new MutationObserver(ns.reloadData);
    observer.observe($("divLista2"), config);

    $("form").addEventListener("click", removeDeleteClassError, true);
    $("classSelect").addEventListener("change", removeDeleteClassError, false);
}
window.onload = createPage;
=======
    function changeBackgroundOfListItem() {
        var listItems = ns.selectAll("li"),
            selected = ns.selectFirst("select").selectedIndex,
            addedClass = ns.selectFirst("select").children[selected].value;
        listItems.forEach(function (x) {
            x.style.backgroundColor = "white";
        });
        ns.selectFirst("li." + addedClass).style.backgroundColor = "yellow";
    }
    addCharset();
    setTitle();
    createClassForm();
    ns.selectFirst("form").addEventListener("click", removeDeleteClassError, false);
    ns.selectFirst("select").addEventListener("change", removeDeleteClassError, false);
    ns.selectFirst("select").addEventListener("change", changeBackgroundOfListItem, false);

    //Mutation observer
    var config = {
            attributes: false,
            childList: true,
            characterData: false
        },
        observer = new MutationObserver(function () {
            ns.reloadList();
        });
    observer.observe(document.querySelector("select"), config);
}
>>>>>>> b02033ea6498be1b84c08751c38365aa5afb6d78
