/*jslint
    browser: true,
    node: true,
 */
/*globals
    MutationObserver,
    document
 */
"use strict";

function $(id) {
    return document.getElementById(id);
}

function Element(valor, clase) {
    this.valor = valor;
    this.clase = clase;
}
var ns = (function (ns) {
    ns.INPUTS = ["valor", "clase"];
    ns.PLACEHOLDER = "Introduzca ";
    ns.TITLE = "Manipulación de documentos a través del DOM";
    ns.SELECT_TITLE = "Lista de elementos";
    ns.ADD_BUTTON_VALUE = "Añadir";
    ns.DELETE_BUTTON_VALUE = "Eliminar";
    ns.TABLE_BUTTON_VALUE = "Tabla";
    ns.INPUTS_WITHOUT_VALUE = "Tiene que rellenar ambos campos";
    ns.NO_INDEX_SELECTED = "No hay ninguna clase para borrar";
    ns.OBJECTS = [];

    ns.objectCount = function (clase, valor) {
        return ns.OBJECTS.reduce(function (x, y) {
            return (y.clase === clase && y.valor === valor) ? x + 1 : x;
        }, 0);
    };
    ns.changeColors = function (td, numOfObjects) {
        var color;
        if (numOfObjects < 2) {
            color = "red";
        } else {
            if (numOfObjects > 2) {
                color = "green";
            } else {
                color = "inherit";
            }
        }
        td.style.color = color;
    };
    ns.reloadTable = function () {
        var divTable = $("listaElementos").cloneNode(false),
            newTable = ns.create("table", "lista", "", "", divTable),
            clases = ns.unique("clase"),
            values = ns.unique("valor"),
            headers = ns.create("tr", "headers", "", "", newTable),
            tr,
            numOfObjects,
            td;
        ns.create("td", "", "", "", headers);
        clases.forEach(function (x) {
            ns.create("td", "", "", x, headers);
        });

        values.forEach(function (value) {
            tr = ns.create("tr", "", "", value, newTable);
            clases.forEach(function (clase) {
                numOfObjects = ns.objectCount(clase, value);
                td = ns.create("td", "", "", numOfObjects, tr);
                ns.changeColors(td, numOfObjects);
            });
        });
        $("divLista").replaceChild(divTable, $("listaElementos"));
    };
    ns.reloadList = function () {
        var divUl = $("listaElementos").cloneNode(false),
            newUl = ns.create("ul", "lista", "", "", divUl);
        ns.OBJECTS.forEach(function (x) {
            ns.create("li", "", x.clase, x.valor, newUl);
        });
        $("divLista").replaceChild(divUl, $("listaElementos"));
    };
    ns.unique = function (type) {
        return ns.OBJECTS.map(function (x) {
            return x[type];
        }).filter(function (element, index, array) {
            return index === array.indexOf(element);
        });
    };
    ns.reloadData = function () {
        return $("tableList").value === "Tabla" ? ns.reloadList() : ns.reloadTable();
    };
    ns.create = function (tag, id, clase, text, parent) {
        var element = document.createElement(tag);
        element.id = id;
        element.className = clase;
        element.appendChild(document.createTextNode(text));
        parent.appendChild(element);
        return element;
    };
    ns.save = function (valor, clase) {
        return ns.OBJECTS.push(new Element(valor, clase));
    };
    ns.delete = function (clase) {
        ns.OBJECTS = ns.OBJECTS.filter(function (x) {
            return x.clase !== clase;
        });
    };
    ns.selectAll = function (tag) {
        return Array.from(document.querySelectorAll(tag));
    };
    ns.selectFirst = function (tag) {
        return Array.from(document.querySelector(tag));
    };
    ns.changeDataView = function () {
        if ($("tableList").value === "Tabla") {
            ns.reloadTable();
            $("tableList").value = "Lista";
        } else {
            ns.reloadList();
            $("tableList").value = "Tabla";
        }
    };
    return ns;
}({}));

function setTitle() {
    ns.create("h1", "title", "", ns.TITLE, document.body);
}

function appendOption(objects) {
    //Con true coge todos sus children
    var newSelect = document.getElementsByTagName("select")[0].cloneNode(false);
    objects.forEach(function (x) {
        ns.create("option", "", x, x, newSelect);
    });
    $("divLista2").replaceChild(newSelect, $("classSelect"));
}

function deleteSelectedOption() {
    var select = document.getElementsByTagName("select")[0],
        selected = select.selectedIndex;
    if (selected !== -1) {
        ns.delete(select[selected].className);
        appendOption(ns.unique("clase"));
    } else {
        ns.create("p", "", "", ns.NO_INDEX_SELECTED, $("form"));
    }
}

function createDeleteAndTableButtons() {
    var deleteBtn = ns.create("input", "delete", "", "Eliminar", $("divLista")),
        tableBtn;
    deleteBtn.setAttribute("type", "button");
    tableBtn = ns.create("input", "tableList", "", "Tabla", $("divLista"));
    deleteBtn.setAttribute("type", "button");
    tableBtn.setAttribute("type", "button");
    deleteBtn.value = ns.DELETE_BUTTON_VALUE;
    tableBtn.value = ns.TABLE_BUTTON_VALUE;
    deleteBtn.addEventListener("click", deleteSelectedOption, false);
    tableBtn.addEventListener("click", ns.changeDataView, false);
}

function insertIntoList() {
    var valueInput = document.forms[0].children[0].value,
        classInput = document.forms[0].children[1].value;
    if (valueInput !== "" && classInput !== "") {
        ns.save(valueInput, classInput);
        appendOption(ns.unique("clase"));
        document.forms[0].reset();
    } else {
        ns.create("p", "", "", ns.INPUTS_WITHOUT_VALUE, $("form"));
    }
}

function createSelectAndList() {
    var div = ns.create("div", "divLista", "", "", document.body),
        div2 = ns.create("div", "divLista2", "", "", div);
    ns.create("h2", "listaTitle", "", ns.SELECT_TITLE, $("divLista"));
    ns.create("select", "classSelect", "", "", div2);

    ns.create("div", "listaElementos", "", "", $("divLista"));
    ns.create("ul", "lista", "", "", $("listaElementos"));
    createDeleteAndTableButtons();
}

function createClassForm() {
    var form = ns.create("form", "form", "", "", document.body),
        button,
        input;
    ns.INPUTS.forEach(function (x) {
        input = ns.create("input", x, "", "", $("form"));
        input.placeholder = ns.PLACEHOLDER + x;
    });
    button = ns.create("input", "add", "", "Añadir", form);
    button.setAttribute("type", "button");
    button.value = ns.ADD_BUTTON_VALUE;
    button.addEventListener("click", insertIntoList, false);
    createSelectAndList();
}

function removeDeleteClassError() {
    if (ns.selectAll("p")) {
        Array.from(ns.selectAll("p")).forEach(function (x) {
            x.remove();
        });
    }
}

function createPage() {
    setTitle();
    createClassForm();
    var config = {
            attributes: true,
            childList: true,
            characterData: true
        },
        observer;
    observer = new MutationObserver(ns.reloadData);
    observer.observe($("divLista2"), config);

    $("form").addEventListener("click", removeDeleteClassError, true);
    $("classSelect").addEventListener("change", removeDeleteClassError, false);
}
window.onload = createPage;